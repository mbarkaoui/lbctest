//
//  AppDelegate.swift
//  LBCTest
//
//  Created by Malek BARKAOUI on 19/10/2020.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        self.window = UIWindow(frame: UIScreen.main.bounds)
        
        window?.rootViewController = ItemsListRouter.createItemsListModule()
        
        window?.makeKeyAndVisible()
 
        return true
    }

}

