//
//  ImagesPreviewCell.swift
//  LBCTest
//
//  Created by Malek BARKAOUI on 19/10/2020.
//

import UIKit

import UIKit
class ImagesPreviewCell : UIView{
    
    private let photosNumberLabel : UILabel = {
        let lbl = UILabel()
        lbl.textColor = .white
        lbl.font = UIFont.boldSystemFont(ofSize: 12)
        lbl.text = "0"
        
        return lbl
    }()
    
    private let pictoImageView : UIImageView = {
        let imgView = UIImageView(image: #imageLiteral(resourceName: "photo"))
        imgView.contentMode = .scaleToFill
        return imgView
    }()
    
    private let itemImageView : UIImageView = {
        let imgView = UIImageView(image: #imageLiteral(resourceName: "placeholder-image"))
        imgView.contentMode = .scaleToFill
        return imgView
    }()
    
    public let containerImageIndicatorView : UIView = {
        let containerView = UIView()
        containerView.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        containerView.layer.cornerRadius = 15
        
        return containerView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public func setupContainerImageIndicatorView() {
        self.containerImageIndicatorView.isHidden = false
    }
    
    public func configure(item:Item){
        // si je recupére un array d'images je peux determiner le nombre
        // d'images avec .count sur l'array et j'afficherai par la suite
        // le nombre d'images sur la cellule dans notre cas on a qu'une seule image
        // c'est pour ça que je lui affecte toujours "1"
        
        //var photoCount = item.photos.count
        
        photosNumberLabel.text = "1"
        itemImageView.load(urlString: item.images_url.small ?? "", completion: { errorLoading in
            if errorLoading {
                DispatchQueue.main.async {
                    self.containerImageIndicatorView.isHidden =  true
                }
            }
        })
    }
    
    func setupView() {
        
        self.layer.cornerRadius = 10
        self.layer.maskedCorners = [.layerMinXMaxYCorner,.layerMinXMinYCorner]
        self.clipsToBounds = true
        
        addSubview(itemImageView)
        itemImageView.addSubview(containerImageIndicatorView)
        containerImageIndicatorView.addSubview(pictoImageView)
        containerImageIndicatorView.addSubview(photosNumberLabel)
        
        itemImageView.anchor(top: topAnchor, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, paddingTop: 0, paddingLeft: 0 , paddingBottom: 0, paddingRight: 0, width: 0, height: 0, enableInsets: false)
        containerImageIndicatorView.anchor(top: nil, left: itemImageView.leftAnchor, bottom: itemImageView.bottomAnchor, right: nil, paddingTop: 0, paddingLeft: 6, paddingBottom: 6, paddingRight: 0, width: 50, height: 30, enableInsets: false)
        pictoImageView.centerYAnchor.constraint(equalTo: containerImageIndicatorView.centerYAnchor).isActive = true
        pictoImageView.anchor(top: nil, left: containerImageIndicatorView.leftAnchor, bottom: nil, right: nil, paddingTop: 0, paddingLeft: 6, paddingBottom: 0, paddingRight: 0, width: 20, height: 20, enableInsets: false)
        
        photosNumberLabel.centerYAnchor.constraint(equalTo: containerImageIndicatorView.centerYAnchor).isActive = true
        photosNumberLabel.anchor(top: nil, left: nil, bottom: nil, right: containerImageIndicatorView.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 5, width: 10, height: 10, enableInsets: false)
        
        
        
    }
}

