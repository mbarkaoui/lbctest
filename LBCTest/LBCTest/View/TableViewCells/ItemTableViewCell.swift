//
//  ItemTableViewCell.swift
//  LBCTest
//
//  Created by Malek BARKAOUI on 19/10/2020.
//


import UIKit
class ItemTableViewCell : UITableViewCell {
    
    public var item : Item? {
        didSet {
            itemNameLabel.text = item?.title
            itemPriceLabel.text = "\(String(describing: item!.price)) €"
            itemDateLabel.text = item?.creationDate
            itemCategoryLabel.text = item?.categoryName
            self.urgentImage.isHidden = ((item?.isUrgent) == false)
            previewImageCell.configure(item: item!)
        }
    }
    
    private let previewImageCell : ImagesPreviewCell = ImagesPreviewCell()
    
    private let containerView : UIView = {
        let container = UIView()
        return container
    }()
    
    
    
    private let urgentImage : UIImageView = {
        let imgView = UIImageView(image:#imageLiteral(resourceName: "urgentPicto"))
        imgView.contentMode = .scaleAspectFit
        imgView.clipsToBounds = true
        return imgView
    }()
    
    private let itemNameLabel : UILabel = {
        let lbl = UILabel()
        lbl.textColor = .black
        lbl.font = UIFont.boldSystemFont(ofSize: 15)
        lbl.lineBreakMode = .byTruncatingTail
        lbl.numberOfLines = 0
        
        return lbl
    }()
    
    
    private let itemPriceLabel : UILabel = {
        let lbl = UILabel()
        lbl.textColor = #colorLiteral(red: 1, green: 0.4303138256, blue: 0.07785933465, alpha: 1)
        lbl.font = UIFont.systemFont(ofSize: 13)
        lbl.textAlignment = .left
        
        lbl.numberOfLines = 0
        return lbl
    }()
    
    
    private let itemDateLabel : UILabel = {
        let lbl = UILabel()
        lbl.textColor = .black
        lbl.font = UIFont.systemFont(ofSize: 13)
        lbl.textAlignment = .left
        
        lbl.numberOfLines = 0
        return lbl
    }()
    
    private let itemCategoryLabel : UILabel = {
        let lbl = UILabel()
        lbl.textColor = .black
        lbl.font = UIFont.systemFont(ofSize: 13)
        lbl.textAlignment = .left
        
        lbl.numberOfLines = 0
        return lbl
    }()
    
    private let itemImage : UIImageView = {
        let imgView = UIImageView()
        imgView.contentMode = .scaleAspectFit
        imgView.clipsToBounds = true
        return imgView
    }()
    
    override func prepareForReuse() {
        self.previewImageCell.setupContainerImageIndicatorView()
        self.urgentImage.isHidden = true
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        if selected {
            self.isSelected = false
        }
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        addSubview(containerView)
        previewImageCell.layer.cornerRadius = 10
        previewImageCell.layer.maskedCorners = [.layerMinXMaxYCorner,.layerMinXMinYCorner]
        previewImageCell.clipsToBounds = true
        containerView.addSubview(previewImageCell)
        containerView.dropShadow()
        containerView.addSubview(urgentImage)
        containerView.addSubview(itemNameLabel)
        containerView.addSubview(itemPriceLabel)
        containerView.addSubview(itemDateLabel)
        containerView.addSubview(itemCategoryLabel)
        
        containerView.anchor(top: topAnchor, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, paddingTop: 10, paddingLeft: 10, paddingBottom: 10, paddingRight: 10, width: 0, height: 0, enableInsets: false)
        
        urgentImage.anchor(top: containerView.topAnchor, left: nil, bottom: nil, right: containerView.rightAnchor, paddingTop: -1, paddingLeft: 0, paddingBottom: 0, paddingRight: 10, width:30, height: 30, enableInsets: false)
        
        previewImageCell.anchor(top: containerView.topAnchor, left: containerView.leftAnchor, bottom: containerView.bottomAnchor, right: nil, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 5, width: 140, height: 0, enableInsets: false)
        
        itemNameLabel.anchor(top: containerView.topAnchor, left: previewImageCell.rightAnchor, bottom: nil, right: containerView.rightAnchor, paddingTop: 5, paddingLeft: 10, paddingBottom: 0, paddingRight: 40, width: 0, height: 0, enableInsets: false)
        let heightConstraint = NSLayoutConstraint(
                item: itemNameLabel,
                attribute: .height,
                relatedBy: .lessThanOrEqual,
                toItem: nil,
                attribute: .notAnAttribute,
                multiplier: 1.0,
                constant: 60
        )
        
        itemNameLabel.addConstraint(heightConstraint)
        
        itemPriceLabel.anchor(top: itemNameLabel.bottomAnchor, left: previewImageCell.rightAnchor, bottom: nil, right: containerView.rightAnchor, paddingTop: 5, paddingLeft: 10, paddingBottom: 0, paddingRight: 0, width: 0, height: 0, enableInsets: false)
        
        itemDateLabel.anchor(top: nil, left: previewImageCell.rightAnchor, bottom: bottomAnchor, right: containerView.rightAnchor, paddingTop: 10, paddingLeft: 10, paddingBottom: 10, paddingRight: 0, width: 0, height: 0, enableInsets: false)
        
        itemCategoryLabel.anchor(top: nil, left: previewImageCell.rightAnchor, bottom: itemDateLabel.topAnchor, right: containerView.rightAnchor, paddingTop: 0, paddingLeft: 10, paddingBottom: 5, paddingRight: 0, width: 0, height: 0, enableInsets: false)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
}

