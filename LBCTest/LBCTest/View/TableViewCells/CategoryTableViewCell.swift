//
//  CategoryTableViewCell.swift
//  LBCTest
//
//  Created by Malek BARKAOUI on 20/10/2020.
//

import UIKit

class CategoryTableViewCell : UITableViewCell {
    
    public var category : CategoryItem? {
        didSet {
            //productImage.image = product?.productImage
            categoryNameLabel.text = category?.name
        }
    }
    
    
    private let containerView : UIView = {
        let container = UIView()
        container.layer.borderWidth = 1
        container.layer.borderColor = #colorLiteral(red: 1, green: 0.4303138256, blue: 0.07785933465, alpha: 1).cgColor
        return container
    }()
    
    
    private let categoryNameLabel : UILabel = {
        let lbl = UILabel()
        lbl.textColor = #colorLiteral(red: 1, green: 0.4303138256, blue: 0.07785933465, alpha: 1)
        lbl.font = UIFont.boldSystemFont(ofSize: 15)
        
        lbl.numberOfLines = 0
        
        return lbl
    }()
    
    public func setSelectedCategory(){
        self.containerView.backgroundColor = #colorLiteral(red: 1, green: 0.4303138256, blue: 0.07785933465, alpha: 1)
        self.categoryNameLabel.textColor = .white
    }
    
    override func prepareForReuse() {
        categoryNameLabel.textColor = #colorLiteral(red: 1, green: 0.4303138256, blue: 0.07785933465, alpha: 1)
        containerView.dropShadowrounded()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        if selected {
            self.isSelected = false
        }
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        addSubview(containerView)
        containerView.addSubview(categoryNameLabel)
        containerView.anchor(top: topAnchor, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, paddingTop: 15, paddingLeft: 30, paddingBottom: 15, paddingRight: 30, width: 0, height: 0, enableInsets: false)
        containerView.dropShadowrounded()
        categoryNameLabel.translatesAutoresizingMaskIntoConstraints = false

        categoryNameLabel.centerXAnchor.constraint(equalTo: containerView.centerXAnchor).isActive = true

        categoryNameLabel.centerYAnchor.constraint(equalTo: containerView.centerYAnchor).isActive = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
}




