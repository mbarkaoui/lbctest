//
//  CategoryService.swift
//  LBCTest
//
//  Created by Malek BARKAOUI on 22/10/2020.
//

import Foundation

class CategoryService: NSObject {
    
    func getCategoriesFromUrl(completionHandler: @escaping ([CategoryItem]?, Error?) -> Void) {
        var categories = [CategoryItem]()
    if let url = URL(string: "https://raw.githubusercontent.com/leboncoin/paperclip/master/categories.json") {
        let session = URLSession.shared
        session.dataTask(with: url){ data, response, error in
          if let data = data {
              do {
                 let res = try JSONDecoder().decode([CategoryItem].self, from: data)
                categories = res
                completionHandler(categories,nil)
              } catch _ {
                completionHandler(nil,ServiceError.jsonEncode)
              }
           }
       }.resume()
        
    }
}
}
    
   
