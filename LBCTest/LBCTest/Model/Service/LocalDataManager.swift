//
//  LocalDataManager.swift
//  LBCTest
//
//  Created by Malek BARKAOUI on 23/10/2020.
//

import Foundation


class LocalDataManager : NSObject{

    func getCategories() -> [CategoryItem]? {
        do {
            return try getData(name: "categoriesMock")
        } catch {
            return nil
        }
    }
    
    func getItems() -> [Item]? {
        do {
            return try getData(name: "itemsMock")
        } catch {
            return nil
        }
    }
    
private func getData<T>(name: String) throws -> T? where T:Decodable {
    do {
        if let listData = self.readLocalFile(forName: name){
            return  try parse(jsonData: listData)
        }
    } catch {
        print("problem to get Data")
    }
    
    return nil
}

private func readLocalFile(forName name: String) -> Data? {
    do {
        if let bundlePath = Bundle.main.path(forResource: name,
                                             ofType: "geojson"),
            let jsonData = try String(contentsOfFile: bundlePath).data(using: .utf8) {
            return jsonData
        }
    } catch {
        print(error)
    }
    return nil
}

private func parse<T>(jsonData: Data) throws -> T where T:Decodable {
    return try JSONDecoder().decode(T.self, from: jsonData)
}
}
