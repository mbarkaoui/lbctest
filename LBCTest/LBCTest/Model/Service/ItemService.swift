//
//  ItemService.swift
//  LBCTest
//
//  Created by Malek BARKAOUI on 19/10/2020.
//

import Foundation


enum ServiceError: Error {
    case parameters(reason: String)
    case connection
    case jsonEncode
    case timeOut
    case internalError(reason: String)
    case objectSerialization(reason: String)
}

class ItemService: NSObject {
    
    func getItemsFromUrl(completionHandler: @escaping ([Item]?, Error?) -> Void) {
        var items = [Item]()
        if let url = URL(string: "https://raw.githubusercontent.com/leboncoin/paperclip/master/listing.json") {
            let session = URLSession.shared
            session.dataTask(with: url){ data, response, error in
                if let data = data {
                    print("this is data \(data)")
                    do {
                        let res = try JSONDecoder().decode([Item].self, from: data)
                        print("this is itemList \(res)")
                        items = res
                        completionHandler(items,nil)
                    } catch _ {
                        completionHandler(nil,ServiceError.jsonEncode)
                    }
                }
            }.resume()
            
        }
    }
}



