//
//  Item.swift
//  LBCTest
//
//  Created by Malek BARKAOUI on 19/10/2020.
//

import Foundation

struct Item: Decodable {
    let id: Int
    let categoryID: Int
    var categoryName:String
    let title: String
    let description: String
    let price: Double
    let images_url: ImagesUrl
    var creationDate: String
    let isUrgent: Bool
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case categoryID = "category_id"
        case title = "title"
        case description = "description"
        case price = "price"
        case images_url = "images_url"
        case creationDate = "creation_date"
        case isUrgent = "is_urgent"
    }
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try container.decode(Int.self, forKey: .id)
        self.categoryID = try container.decode(Int.self, forKey: .categoryID)
        self.title = try container.decode(String.self, forKey: .title)
        self.description = try container.decode(String.self, forKey: .description)
        self.price = try container.decode(Double.self, forKey: .price)
        self.images_url = try container.decode(ImagesUrl.self, forKey:.images_url)
        self.creationDate = try container.decode(String.self, forKey: .creationDate)
        self.isUrgent = try container.decode(Bool.self, forKey: .isUrgent)
        self.categoryName = ""
    }
    
    
    mutating func addCategory(categoryName:String) {
        self.categoryName = categoryName
    }
    
    mutating func reformatCreationDate(creationDate:String) {
        self.creationDate = creationDate
    }
    
}

