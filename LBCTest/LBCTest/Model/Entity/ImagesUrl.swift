//
//  ImagesUrl.swift
//  LBCTest
//
//  Created by Malek BARKAOUI on 19/10/2020.
//

import Foundation

struct ImagesUrl: Decodable {
    
    let small: String?
    let thumb: String?
    
}
