//
//  CategoryItem.swift
//  LBCTest
//
//  Created by Malek BARKAOUI on 19/10/2020.
//

import Foundation

struct CategoryItem: Codable {
    let id: Int
    let name: String
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case name = "name"
    }
}
