//
//  UIStringExtension.swift
//  LBCTest
//
//  Created by Malek BARKAOUI on 19/10/2020.
//

import UIKit

extension String {
         func getFormattedDate(string: String , formatter:String) -> String{
            let dateFormatterGet = DateFormatter()
            dateFormatterGet.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
            let dateFormatterPrint = DateFormatter()
            dateFormatterPrint.dateFormat = "dd MMMM  HH:mm "
            let date: Date? = dateFormatterGet.date(from: self)
            if let dateformat = date {
                return dateFormatterPrint.string(from:dateformat)
            } else {
                return ""
            }
            // Feb 01,2018
            
        }
}
