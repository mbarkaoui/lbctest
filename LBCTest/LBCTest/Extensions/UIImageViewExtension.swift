//
//  UIImageViewExtension.swift
//  LBCTest
//
//  Created by Malek BARKAOUI on 19/10/2020.
//

import UIKit


extension UIImageView {
    
    func load(urlString: String,  completion:  @escaping ((Bool) -> Void)) {
        guard let imageUrl = URL(string: urlString) else { return }
        let activityIndicator = UIActivityIndicatorView()
        activityIndicator.frame = CGRect.init(x: 0, y: 0, width: self.frame.size.width, height: self.frame.size.height)
        activityIndicator.startAnimating()
        if self.image == nil{
            self.addSubview(activityIndicator)
        }
        DispatchQueue.global().async { [weak self] in
            if let data = try? Data(contentsOf: imageUrl) {
                if let image = UIImage(data: data) {
                    DispatchQueue.main.async {
                        self?.image = image
                        activityIndicator.removeFromSuperview()
                    }
                }
            } else {
                DispatchQueue.main.async {
                    self?.image = UIImage(named: "placeholder-image")
                }
                completion(true)
            }
        }
    }
    
    public func imageFromURL(urlString: String) {
        
        let activityIndicator = UIActivityIndicatorView()
        activityIndicator.frame = CGRect.init(x: 0, y: 0, width: self.frame.size.width, height: self.frame.size.height)
        activityIndicator.startAnimating()
        if self.image == nil{
            self.addSubview(activityIndicator)
        }
        
        URLSession.shared.dataTask(with: NSURL(string: urlString)! as URL, completionHandler: { (data, response, error) -> Void in
            
            if error != nil {
                print(error ?? "No Error")
                return
            }
            DispatchQueue.main.async(execute: { () -> Void in
                let image = UIImage(data: data!)
                activityIndicator.removeFromSuperview()
                self.image = image
            })
            
        }).resume()
    }
}
