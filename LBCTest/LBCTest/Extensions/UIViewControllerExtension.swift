//
//  UIViewControllerExtension.swift
//  LBCTest
//
//  Created by Malek BARKAOUI on 21/10/2020.
//

import UIKit

extension UIViewController {
    private static var alert = UIAlertController()
    func presentloadingAlert() {
        UIViewController.alert = UIAlertController(title: nil, message: NSLocalizedString("loading", comment: ""), preferredStyle: .alert)

       let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
       loadingIndicator.hidesWhenStopped = true
       loadingIndicator.startAnimating();

        UIViewController.alert.view.addSubview(loadingIndicator)
       present(UIViewController.alert, animated: true, completion: nil)
    }
    func dismissloadingAlert() {
        UIViewController.alert.dismiss(animated: true, completion: nil)
    }
}
