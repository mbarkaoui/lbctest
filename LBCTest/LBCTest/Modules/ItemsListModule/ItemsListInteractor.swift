//
//  itemsListInteractor.swift
//  LBCTest
//
//  Created by Malek BARKAOUI on 19/10/2020.
//

import Foundation

class  ItemsListInteractor: PresenterToInteractorItemsListProtocol {
    var presenter: InteractorToPresenterItemsListProtocol?
    
    func loadItems(categoryId:Int?) {
        
        let itemService = ItemService()
        itemService.getItemsFromUrl{ items,error in
                        print("interactor working...")
                        if error == nil {
                            self.presenter?.itemsSuccess(itemList: items!,categoryId: categoryId)
                        }else{
                            self.presenter?.itemsFailed()
                        }
        }
    }
    
    func loadCategories() {
        let categoryService = CategoryService()
        categoryService.getCategoriesFromUrl{ categories,error in
                        print("interactor working...")
                        if error == nil {
                            self.presenter?.categoriesSuccess(categories: categories!)
                        }else{
                            self.presenter?.itemsFailed()
                        }
        }
    }
    

}
