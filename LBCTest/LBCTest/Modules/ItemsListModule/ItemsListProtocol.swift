//
//  itemsListProtocol.swift
//  LBCTest
//
//  Created by Malek BARKAOUI on 19/10/2020.
//

import UIKit

protocol ViewToPresenterItemsListProtocol:class {
    
    var view: PresenterToViewItemsListProtocol? {get set}
    var interactor: PresenterToInteractorItemsListProtocol? {get set}
    var router: PresenterToRouterItemsListProtocol? {get set}
    
    var selectedCategory:Int? {get set}
    func fetchItems(categoryId:Int?)
    func fetchItemsforCategories(categoryId:Int, listItems:[Item])
    func gotoListCategories()
    func showItemDetail(item:Item)
}

protocol PresenterToViewItemsListProtocol:class {
    func onItemFilterSuccess(filtredList:Array<Item>)
    func onItemResponseSucces(itemsList: Array<Item>)
    func onItemResponseFailed(error: String)
}

protocol PresenterToRouterItemsListProtocol:class {
    static func createItemsListModule() -> UINavigationController
    func presentCategoryDetail(on view: PresenterToViewItemsListProtocol, categoryId:Int?)
    func presentItemDetail(on view: PresenterToViewItemsListProtocol,item:Item)
}

protocol PresenterToInteractorItemsListProtocol:class {
    
    var presenter: InteractorToPresenterItemsListProtocol? {get set}
    func loadItems(categoryId:Int?)
    func loadCategories()
}

protocol InteractorToPresenterItemsListProtocol:class {
    func itemsSuccess(itemList: Array<Item>,categoryId:Int?)
    func categoriesSuccess(categories: Array<CategoryItem>)
    func itemsFailed()
}
