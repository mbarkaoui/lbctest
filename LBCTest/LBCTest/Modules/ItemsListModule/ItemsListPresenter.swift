//
//  ItemsListPresenter.swift
//  LBCTest
//
//  Created by Malek BARKAOUI on 19/10/2020.
//

import Foundation

class ItemsListPresenter: ViewToPresenterItemsListProtocol {

    var view: PresenterToViewItemsListProtocol?
    
    var interactor: PresenterToInteractorItemsListProtocol?
    
    var router: PresenterToRouterItemsListProtocol?
    
    var items:[Item]?
    var categoryId:Int?
    public var selectedCategory: Int?

    func fetchItems(categoryId:Int? = nil ) {
        print("Presenting...")
        interactor?.loadItems(categoryId: categoryId)
    }
    
    func fetchItemsforCategories(categoryId: Int, listItems:[Item]) {
        let filtredItems = listItems.filter{
            $0.categoryID == categoryId
        }
        
        view?.onItemFilterSuccess(filtredList: filtredItems)
    }
    
    
    func fetchCategories() {
        interactor?.loadCategories()
    }
    
    func showItemDetail(item: Item) {
        router?.presentItemDetail(on: view!, item: item)
    }
    
    func gotoListCategories() {
        
        router?.presentCategoryDetail(on: view!, categoryId: self.selectedCategory)
    }
    
    
}

extension ItemsListPresenter: InteractorToPresenterItemsListProtocol {
    
    func transformCategoriesArrayToDictionnary(categories:[CategoryItem]) -> NSMutableDictionary {
        let categoryDic = NSMutableDictionary()
        categories.forEach{ categorie in
            categoryDic[String(categorie.id)] = categorie.name
        }
        return categoryDic
    }
    
    func addCategoryReformatDate(items:[Item],categoryDic:NSMutableDictionary) -> [Item] {
        
        let reformatedItems = items.map { item -> Item in
            var itemModify = item
            if let categoryName = categoryDic.value(forKey: item.categoryID.description) as? String {
                itemModify.addCategory(categoryName: categoryName)
                itemModify.reformatCreationDate(creationDate: itemModify.creationDate.getFormattedDate(string: "", formatter: ""))
            }
            return itemModify
        }
        
        return reformatedItems
    }
    
    func sortedUrgentItems(items:[Item]) -> [Item] {
        return items.filter{
            $0.isUrgent == true }
            .sorted{ $0.creationDate > $1.creationDate}
    }
    
    func sortedNotUrgentItems(items:[Item]) -> [Item] {
        return items.filter{
            $0.isUrgent == false }
            .sorted{ $0.creationDate > $1.creationDate}
    }
    
    func getItemsWithCategory(items: [Item],categoryId: Int) -> [Item] {
        let  filtredIdtems = items.filter({
            $0.categoryID == categoryId
        })

        return filtredIdtems
    }
    
    func appendUrgentNotUrgent(urgentItemList: [Item], notUrgentItems: [Item]) -> [Item]{
        var urgentNotUrgentItems = urgentItemList
        urgentNotUrgentItems.append(contentsOf: notUrgentItems)
        return urgentNotUrgentItems
    }
    
    func categoriesSuccess(categories: Array<CategoryItem>) {
        // transform dataArray to Dictionnary
        guard let items = self.items else{
            return
        }
        
        let categoryDic = self.transformCategoriesArrayToDictionnary(categories: categories)
        
        // add categoryName & reformat creation date
        self.items = self.addCategoryReformatDate(items: items, categoryDic: categoryDic)

        // sorted Urgent Items & tri urgent Items
        let sortedUrgentItems = self.sortedUrgentItems(items: self.items ?? [Item]())
        print("urgent items \(String(describing: sortedUrgentItems.count))")
        
        // sorted Not Urgent Items & tri urgent Items
        let sortedNotUrgentItems = self.sortedNotUrgentItems(items: self.items ?? [Item]())
        print("No urgent items \(String(describing: sortedNotUrgentItems.count))")

        // append urgen items to non urgent items
        var finalFiltredItems = appendUrgentNotUrgent(urgentItemList: sortedUrgentItems, notUrgentItems: sortedNotUrgentItems)
        
        if let categoryId = categoryId {
             finalFiltredItems =  self.getItemsWithCategory(items: finalFiltredItems, categoryId: categoryId)
        }
        
        view?.onItemResponseSucces(itemsList: finalFiltredItems)
    }
    
    func itemsSuccess(itemList: Array<Item>, categoryId:Int?) {
        self.items = itemList
        self.categoryId = categoryId
        fetchCategories()
    }
    
    func itemsFailed() {
        view?.onItemResponseFailed(error: "Parsing Error")
    }
    
}
