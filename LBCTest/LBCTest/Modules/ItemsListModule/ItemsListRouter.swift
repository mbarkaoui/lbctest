//
//  itemsListRouter.swift
//  LBCTest
//
//  Created by Malek BARKAOUI on 19/10/2020.
//

import Foundation
import UIKit

class ItemsListRouter: PresenterToRouterItemsListProtocol {
    
    class func createItemsListModule() -> UINavigationController {
        print("Routing")
        
        let viewController = ViewController()
        let navigationController = UINavigationController(rootViewController: viewController)
        navigationController.navigationBar.backgroundColor = #colorLiteral(red: 1, green: 0.4303138256, blue: 0.07785933465, alpha: 1)
        navigationController.navigationBar.barTintColor = #colorLiteral(red: 1, green: 0.4303138256, blue: 0.07785933465, alpha: 1)
        navigationController.navigationBar.tintColor = .white
        viewController.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)

        
        let presenter: ViewToPresenterItemsListProtocol & InteractorToPresenterItemsListProtocol = ItemsListPresenter()
        
        viewController.itemsPresenter = presenter
        viewController.itemsPresenter?.view = viewController
        viewController.itemsPresenter?.router = ItemsListRouter()
        viewController.itemsPresenter?.interactor = ItemsListInteractor()
        viewController.itemsPresenter?.interactor?.presenter = presenter
        
        return navigationController
    }
    
    // MARK: - Navigation
    func presentCategoryDetail(on view: PresenterToViewItemsListProtocol, categoryId: Int?) {
                    
        let viewController = view as! ViewController
        let categoryListViewController = CategoryListRouter.createCategoryListModule(categoryId: categoryId)
        categoryListViewController.delegate = viewController
        viewController.present(categoryListViewController, animated: true, completion: {})
  
    }
    
    func presentItemDetail(on view: PresenterToViewItemsListProtocol, item: Item) {
        let viewController = view as! ViewController
        let ItemDetailViewController = ItemDetailRouter.createItemDetailModule(item: item)
        viewController.modalPresentationStyle = .fullScreen
        viewController.present(ItemDetailViewController, animated: true, completion: {})
        
    }
    

}
