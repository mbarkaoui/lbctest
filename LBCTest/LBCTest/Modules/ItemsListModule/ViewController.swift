//
//  ViewController.swift
//  LBCTest
//
//  Created by Malek BARKAOUI on 19/10/2020.
//

import UIKit

class ViewController: UIViewController {

    var itemsPresenter: ViewToPresenterItemsListProtocol?
    var alert:UIAlertController = UIAlertController()
    var loadingIndicator:UIActivityIndicatorView = UIActivityIndicatorView()
    
    var heightConstraint:NSLayoutConstraint?
    
    private let resetCategoryView : UIView = {
        let container = UIView()
        container.backgroundColor = #colorLiteral(red: 1, green: 0.4303138256, blue: 0.07785933465, alpha: 1)
        return container
    }()
    
    private let selectedCategoryLabel : UILabel = {
        let lbl = UILabel()
        lbl.textColor = .white
        lbl.font = UIFont.boldSystemFont(ofSize: 20)
        lbl.textAlignment = .left
        lbl.numberOfLines = 0
        lbl.text = "Categorie"
        return lbl
    }()
 
    let tableView = UITableView()
    private let allCategoriesButton : UIButton = {
        let btn = UIButton()
        
        btn.setBackgroundColor(#colorLiteral(red: 1, green: 0.4303138256, blue: 0.07785933465, alpha: 1), for: .normal)
        btn.setBackgroundColor(UIColor.systemOrange, for: .highlighted)
        btn.titleLabel?.font = .systemFont(ofSize: 12)
        btn.setTitle("Toutes les catégories", for: .normal)
        btn.layer.cornerRadius = 15
        btn.layer.borderWidth = 1
        btn.clipsToBounds = true
        
        btn.addTarget(self, action:
                     #selector(ViewController.handleResetFilter(_:)),
                      for: UIControl.Event.touchDown)
        return btn
    }()
    
    private let floatingParametersButton : UIButton = {
        let btn = UIButton()
        btn.backgroundColor = #colorLiteral(red: 1, green: 0.4303138256, blue: 0.07785933465, alpha: 1)
        btn.imageEdgeInsets = UIEdgeInsets(top: 10.0, left: 10.0, bottom: 10.0, right: 10.0)
        
        btn.setImage(#imageLiteral(resourceName: "params"), for: .normal)
    
        return btn
    }()
    var safeArea: UILayoutGuide!
    var itemsList:[Item]?
    var itemsListToShow:[Item]?
    var item:Item?
    var categoryDict:NSMutableDictionary?

    lazy var refreshControl: UIRefreshControl = {
            let refreshControl = UIRefreshControl()
            refreshControl.addTarget(self, action:
                         #selector(ViewController.handleRefresh(_:)),
                                     for: UIControl.Event.valueChanged)
        refreshControl.tintColor = #colorLiteral(red: 1, green: 0.4303138256, blue: 0.07785933465, alpha: 1)
            
            return refreshControl
        }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.itemsPresenter?.fetchItems(categoryId: nil)
        self.presentloadingAlert()
    }
    
    override func loadView() {
        super.loadView()
        view.backgroundColor = .white
        safeArea = view.layoutMarginsGuide
        tableView.register(ItemTableViewCell.self, forCellReuseIdentifier: "cellId")
        tableView.addSubview(self.refreshControl)
        
        setupView()
      }
    
    func setupView() {
        setupCategoryView()
        setupSelectedCategoryLabel()
        setupAllCategoriesButton()
        setupTableView()
        setupFloatingButton()
        allCategoriesButton.layer.borderColor = UIColor.white.cgColor
        setupNavBar()
    }

    func setupAllCategoriesButton() {
        resetCategoryView.addSubview(allCategoriesButton)
        allCategoriesButton.centerYAnchor.constraint(equalTo: resetCategoryView.centerYAnchor).isActive = true
        allCategoriesButton.anchor(top: nil, left: nil, bottom: nil, right: resetCategoryView.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 20, width: 130, height: 30, enableInsets: false)
        
       heightConstraint = NSLayoutConstraint(item: resetCategoryView, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: 0)

        resetCategoryView.addConstraint(heightConstraint ?? NSLayoutConstraint())
        heightConstraint?.isActive = true
        
    }

    func setupSelectedCategoryLabel() {
        resetCategoryView.addSubview(selectedCategoryLabel)
        selectedCategoryLabel.centerYAnchor.constraint(equalTo: resetCategoryView.centerYAnchor).isActive = true
        selectedCategoryLabel.anchor(top: nil, left: resetCategoryView.leftAnchor, bottom: nil, right: nil, paddingTop: 0, paddingLeft: 20, paddingBottom: 0, paddingRight: 0, width: 150, height: 30, enableInsets: false)
        
    }
    
    func setupCategoryView() {
        view.addSubview(resetCategoryView)
        resetCategoryView.anchor(top: safeArea.topAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 0, enableInsets: false)

    }
    
    func setupTableView() {
        view.addSubview(tableView)

        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.separatorStyle = .none
        tableView.anchor(top: resetCategoryView.bottomAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 0, enableInsets: false)

        tableView.dataSource = self
        tableView.delegate = self
      }

    
    func setupFloatingButton() {
        view.addSubview(floatingParametersButton)
        floatingParametersButton.addTarget(self, action: #selector(self.pressButton(_:)), for: .touchUpInside)

        floatingParametersButton.anchor(top: nil, left: nil, bottom: view.bottomAnchor , right: view.rightAnchor, paddingTop: 0, paddingLeft: 0 , paddingBottom: 20, paddingRight: 20, width: 50, height: 50, enableInsets: false)
        floatingParametersButton.layer.cornerRadius = 25
        
    }
    func setupNavBar() {
        self.title = NSLocalizedString("company_name", comment: "")
        let textAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
    }
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        self.itemsPresenter?.fetchItems(categoryId: self.itemsPresenter?.selectedCategory)
        }
    @objc func handleResetFilter(_ buttons: UIButton) {
        self.presentloadingAlert()

        DispatchQueue.main.async {
            self.alert.view.addSubview(self.loadingIndicator)
        }
        self.itemsPresenter?.selectedCategory = nil
        self.itemsPresenter?.fetchItems(categoryId: self.itemsPresenter?.selectedCategory)
        heightConstraint?.constant = 0
        resetCategoryView.layoutIfNeeded()
        }
    
    
    @objc func pressButton(_ sender: UIButton){ //<- needs `@objc`
        self.itemsPresenter?.gotoListCategories()
    }
    
}

extension ViewController:PresenterToViewItemsListProtocol {
    func onItemFilterSuccess(filtredList: Array<Item>) {
        self.itemsListToShow = filtredList
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    func onItemResponseSucces(itemsList: Array<Item>) {
        if self.itemsPresenter?.selectedCategory == nil {
            self.itemsList = itemsList
            self.itemsListToShow = self.itemsList

        } else {
            self.itemsListToShow = itemsList

        }
       
        DispatchQueue.main.async {
            self.dismissloadingAlert()
            self.refreshControl.endRefreshing()
            self.tableView.reloadData()
        }
        
    }
    
    func onItemResponseFailed(error: String) {
        
    }
    
    
}

extension ViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let rotationTransform = CATransform3DTranslate(CATransform3DIdentity, 0, 30, 0)
        cell.layer.transform = rotationTransform
        cell.alpha = 0
        UIView.animate(withDuration: 0.50) {
            cell.layer.transform = CATransform3DIdentity
            cell.alpha = 1.0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return self.itemsListToShow?.count ?? 0
  }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let item = self.itemsListToShow?[indexPath.row] else {
            return
        }
        self.itemsPresenter?.showItemDetail(item: item)
    }
    
    
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
    let cell =
        tableView.dequeueReusableCell(withIdentifier: "cellId", for: indexPath) as! ItemTableViewCell
    guard let item = self.itemsListToShow?[indexPath.row] else {
        return UITableViewCell()
    }

    cell.item =  item
    return cell
  }
}

extension ViewController:CategoryListDelegate {
    func sendCategories(category: CategoryItem) {
        guard let itemsToFilter = self.itemsList else {
            return
        }
        
        heightConstraint?.constant = 50
        resetCategoryView.layoutIfNeeded()
    
        
        self.selectedCategoryLabel.text = category.name
      
        self.view.updateConstraints()
        self.itemsPresenter?.selectedCategory =  category.id
        self.itemsPresenter?.fetchItemsforCategories(categoryId:category.id, listItems:itemsToFilter)
    }
    
    
}
