//
//  CategoryListProtocol.swift
//  LBCTest
//
//  Created by Malek BARKAOUI on 20/10/2020.
//


import UIKit

protocol ViewToPresenterCategoryListProtocol:class {
    var view: PresenterToViewCategoryListProtocol? {get set}
    var interactor: PresenterToInteractorCategoryListProtocol? {get set}
    var router: PresenterToRouterCategoryListProtocol? {get set}
    var selectedCategoryId:Int? { get set }

    func didSelectCategory(categoryId:Int?)
    func didGetCategory() -> Int?
    func fetchCategories()
    func dismissCategoryList()
}

protocol PresenterToViewCategoryListProtocol:class {
    func onCategoryResponseSucces(categoriesList: Array<CategoryItem>)
    func onCategoryResponseFailed(error: String)
}

protocol PresenterToRouterCategoryListProtocol:class {
    static func createCategoryListModule(categoryId:Int?) -> CategoryListViewController
    func dismiss(on view: PresenterToViewCategoryListProtocol)
}

protocol PresenterToInteractorCategoryListProtocol:class {
    var presenter: InteractorToPresenterCategoryListProtocol? {get set}
    func loadCategories()
}

protocol InteractorToPresenterCategoryListProtocol:class {
    func categoriesListSuccess(categoriesList: Array<CategoryItem>)
    func categoriesListFailed()
}
