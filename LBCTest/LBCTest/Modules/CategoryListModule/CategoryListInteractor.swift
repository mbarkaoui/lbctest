//
//  CategoryListInteractor.swift
//  LBCTest
//
//  Created by Malek BARKAOUI on 20/10/2020.
//

import Foundation

class  CategoryListInteractor: PresenterToInteractorCategoryListProtocol {
    var presenter: InteractorToPresenterCategoryListProtocol?
    
    func loadCategories() {
        let categoryService = CategoryService()
        categoryService.getCategoriesFromUrl{ categories,error in
            print("interactor working...")
            if error == nil {
                self.presenter?.categoriesListSuccess(categoriesList: categories!)
            }else{
                self.presenter?.categoriesListFailed()
            }
        }
        
    }
    
}
