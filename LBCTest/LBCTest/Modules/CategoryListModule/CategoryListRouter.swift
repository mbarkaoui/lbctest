//
//  CategoryListRouter.swift
//  LBCTest
//
//  Created by Malek BARKAOUI on 20/10/2020.
//

import UIKit

class CategoryListRouter: PresenterToRouterCategoryListProtocol {
    static func createCategoryListModule(categoryId:Int?) -> CategoryListViewController {
        let viewController = CategoryListViewController()
        let presenter: ViewToPresenterCategoryListProtocol & InteractorToPresenterCategoryListProtocol = CategoryListPresenter()
        
        presenter.didSelectCategory(categoryId: categoryId)
        viewController.categoriesPresenter = presenter
        viewController.categoriesPresenter?.view = viewController
        viewController.categoriesPresenter?.interactor = CategoryListInteractor()
        viewController.categoriesPresenter?.router = CategoryListRouter()
        viewController.categoriesPresenter?.interactor?.presenter = presenter

        return viewController
    }
    
    func dismiss(on view: PresenterToViewCategoryListProtocol){
        let viewController = view as! CategoryListViewController
        viewController.dismiss(animated: true, completion: nil)
    }

}
