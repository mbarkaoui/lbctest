//
//  CategoryListViewController.swift
//  LBCTest
//
//  Created by Malek BARKAOUI on 20/10/2020.
//

import UIKit

protocol CategoryListDelegate {
    func sendCategories(category: CategoryItem)
}

class CategoryListViewController: UIViewController {
    var delegate:CategoryListDelegate?
    var categoriesPresenter: ViewToPresenterCategoryListProtocol?
    let tableView = UITableView()
    var safeArea: UILayoutGuide!
    var listCategories:[CategoryItem]?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.categoriesPresenter?.fetchCategories()
    }
    override func loadView() {
        super.loadView()
        tableView.register(CategoryTableViewCell.self, forCellReuseIdentifier: "cellId")
        view.backgroundColor = .white
        safeArea = view.layoutMarginsGuide
        setupTableView()
      }
    
    func setupTableView() {
        view.addSubview(tableView)
  
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.separatorStyle = .none
        tableView.topAnchor.constraint(equalTo: safeArea.topAnchor).isActive = true
        tableView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        tableView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        tableView.dataSource = self
        tableView.delegate = self
      }
}

extension CategoryListViewController:UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.listCategories?.count ?? 0
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell =
            tableView.dequeueReusableCell(withIdentifier: "cellId", for: indexPath) as! CategoryTableViewCell
        guard let category = self.listCategories?[indexPath.row] else {
            return CategoryTableViewCell()
        }
        
        if let categoryId = self.listCategories?[indexPath.row].id {
            if categoriesPresenter?.didGetCategory() == categoryId {
                cell.setSelectedCategory()
            }
        }

        cell.category = category
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        if let category = self.listCategories?[indexPath.row] {
            self.categoriesPresenter?.didSelectCategory(categoryId: category.id)
            self.delegate?.sendCategories(category:category)
        }
        self.categoriesPresenter?.dismissCategoryList()
        }
    }


extension CategoryListViewController:PresenterToViewCategoryListProtocol {

    func onCategoryResponseSucces(categoriesList: Array<CategoryItem>) {
        self.listCategories = categoriesList
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    func onCategoryResponseFailed(error: String) {
    
    }
    
    
}
