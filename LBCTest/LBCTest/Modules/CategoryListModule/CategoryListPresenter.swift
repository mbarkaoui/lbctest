//
//  CategoryListPresenter.swift
//  LBCTest
//
//  Created by Malek BARKAOUI on 20/10/2020.
//


import Foundation

class CategoryListPresenter: ViewToPresenterCategoryListProtocol {

    var view: PresenterToViewCategoryListProtocol?
    
    var interactor: PresenterToInteractorCategoryListProtocol?
    
    var router: PresenterToRouterCategoryListProtocol?
    
    var selectedCategoryId: Int?

    func didSelectCategory(categoryId: Int?) {
        selectedCategoryId = categoryId
    }
    func didGetCategory() -> Int? {
        return selectedCategoryId
    }

    func fetchCategories() {
        self.interactor?.loadCategories()
    }
 
    func dismissCategoryList() {
        self.router?.dismiss(on: view!)
    }
    

}

extension CategoryListPresenter: InteractorToPresenterCategoryListProtocol {
    func categoriesListSuccess(categoriesList: Array<CategoryItem>) {
        print("this is list of categories \(categoriesList)")
        self.view?.onCategoryResponseSucces(categoriesList: categoriesList)
    }
    
    func categoriesListFailed() {
        
    }
    
}
