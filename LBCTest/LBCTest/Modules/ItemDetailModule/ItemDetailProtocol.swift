//
//  ItemDetailProtocol.swift
//  LBCTest
//
//  Created by Malek BARKAOUI on 21/10/2020.
//

import Foundation

protocol ViewToPresenterItemDetailProtocol:class {
    var view: PresenterToViewItemDetailProtocol? {get set}
    var interactor: PresenterToInteractorItemDetailProtocol? {get set}
    var router: PresenterToRouterItemDetailProtocol? {get set}
    var item:Item? { get set }
    func dismissItemDetail()
}

protocol PresenterToViewItemDetailProtocol:class {

}

protocol PresenterToRouterItemDetailProtocol:class {
    static func createItemDetailModule(item:Item?) -> ItemDetailViewController
    func dismiss(on view: PresenterToViewItemDetailProtocol)

}

protocol PresenterToInteractorItemDetailProtocol:class {
    var presenter: InteractorToPresenterItemDetailProtocol? {get set}

}

protocol InteractorToPresenterItemDetailProtocol:class {
   
}
