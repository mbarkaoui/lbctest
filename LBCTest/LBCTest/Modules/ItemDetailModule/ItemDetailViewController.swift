//
//  ItemDetailViewController.swift
//  LBCTest
//
//  Created by Malek BARKAOUI on 21/10/2020.
//

import UIKit

class ItemDetailViewController: UIViewController {

    private let pageControl :UIPageControl = UIPageControl()
    private let scrollView : UIScrollView = UIScrollView()
    var itemDetailPresenter: ViewToPresenterItemDetailProtocol?

    private let photosViewContainer : UIView = {
        let img = UIView()
        return img
    }()
    
    private let itemDescritpionTextView : UITextView = {
      let txtView = UITextView()
        
        txtView.font = UIFont.systemFont(ofSize: 15)
        txtView.textAlignment = .left
        
        return txtView
    }()
    
    
    private let itemCreationDateLabel : UILabel = {
        let lbl = UILabel()
        lbl.textColor = .lightGray
        lbl.font = UIFont.boldSystemFont(ofSize: 13)
        lbl.textAlignment = .left
        lbl.numberOfLines = 0
        
        return lbl
    }()
    
    private let itemNameLabel : UILabel = {
        let lbl = UILabel()
        lbl.textColor = .black
        lbl.font = UIFont.boldSystemFont(ofSize: 20)
        lbl.textAlignment = .left
        lbl.numberOfLines = 0
        
        return lbl
    }()
    
    private let itemPriceLabel : UILabel = {
        let lbl = UILabel()
        lbl.textColor =  #colorLiteral(red: 1, green: 0.4303138256, blue: 0.07785933465, alpha: 1)
        lbl.font = UIFont.boldSystemFont(ofSize: 23)
        lbl.textAlignment = .right
        
        lbl.numberOfLines = 0
        return lbl
    }()
    
    private let closeButton: UIButton = {
      let btn = UIButton()
        btn.setImage(#imageLiteral(resourceName: "icons8-close"),for: .normal)
        btn.addTarget(self, action:
                        #selector(ItemDetailViewController.handleDismissItemDetail(_:)),
                         for: UIControl.Event.touchDown)
        return btn
    }()
    
    var imageArray = [UIImage]()
    var imageURLArray = [String]()
    private let photoImageView: UIImageView = {
        let img = UIImageView()
        return img
    }()


    override func viewDidLoad() {
        super.viewDidLoad()
        print("item to show \(String(describing: itemDetailPresenter?.item?.title))")
    }

    override func loadView() {
        super.loadView()
        view.backgroundColor = .white
       
        //au cas ou on plusieur images on peut les swiper en glissant l'image à gauche et à droite et passer imageArray pour le calcul d'offset du scrollView
        //imageArray = [#imageLiteral(resourceName: "121699100_266385914700142_7585849709555657107_n"),#imageLiteral(resourceName: "121635288_1565910926914812_2953624676202711189_n")]
        setupPhotosViewsContainer()
        setupScrollView()
        setupPageControl()
        setupCloseButton()
        setupItemLabel()
        setupItemPriceLabel()
        setupItemCreationDateLabel()
        setupItemDescriptionTextView()
        
        guard let item = itemDetailPresenter?.item else {
          return
        }
         
        self.itemNameLabel.text = item.title
        self.itemPriceLabel.text = "\(String(describing: item.price)) €"
        self.itemCreationDateLabel.text = item.creationDate
        self.itemDescritpionTextView.text = item.description
        
        imageURLArray.append(item.images_url.thumb ?? "")
        // pour tester le swipe utiliser imageArray qui commenter en haut au lien imageURLArray
        for i in 0..<imageURLArray.count {
            let imageView = UIImageView()
            imageView.contentMode = .scaleToFill
            
            //pareil ici remplacer la ligne au dessous avec
            //imageView.image = imageArray[i]
            imageView.load(urlString: imageURLArray[i], completion: {_ in
                
            })
            let xPosition = self.view.frame.width * CGFloat(i)
            imageView.frame = CGRect(x:xPosition, y:0, width: view.frame.width,height: 300)
            scrollView.contentSize.width = view.frame.width * CGFloat(i+1)
            scrollView.addSubview(imageView)
            //pareil ici remplacer la ligne au dessous avec
            //self.pageControl.numberOfPages = imageArray.count
            self.pageControl.numberOfPages = imageURLArray.count
    }
    }
    
    @objc func handleDismissItemDetail(_ buttons: UIButton) {
        self.itemDetailPresenter?.dismissItemDetail()
        }
    
    func setupItemDescriptionTextView() {
        view.addSubview(itemDescritpionTextView)
        itemDescritpionTextView.anchor(top: itemCreationDateLabel.bottomAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, paddingTop: 5, paddingLeft:19, paddingBottom: 1, paddingRight: 20, width: 0, height: 0, enableInsets: false)
    }
    
    func setupItemCreationDateLabel() {
        view.addSubview(itemCreationDateLabel)
        itemCreationDateLabel.anchor(top: itemNameLabel.bottomAnchor, left: view.leftAnchor, bottom: nil, right: nil, paddingTop: 5, paddingLeft:20, paddingBottom: 0, paddingRight: 0, width: view.frame.width/2, height: 0, enableInsets: false)
    }
    func setupItemPriceLabel() {
        view.addSubview(itemPriceLabel)
        itemPriceLabel.anchor(top: photosViewContainer.bottomAnchor, left: nil, bottom: nil, right: view.rightAnchor, paddingTop: 20, paddingLeft: 0, paddingBottom: 0, paddingRight: 20, width: view.frame.width/2, height: 0, enableInsets: false)
        
    }
    
    func setupItemLabel() {
        view.addSubview(itemNameLabel)
        itemNameLabel.anchor(top: photosViewContainer.bottomAnchor, left: view.leftAnchor, bottom: nil, right: nil, paddingTop: 20, paddingLeft: 20, paddingBottom: 0, paddingRight: 0, width: view.frame.width/2, height: 0, enableInsets: false)
    }
    func setupCloseButton(){
        closeButton.layer.cornerRadius = 23
        closeButton.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        photosViewContainer.addSubview(closeButton)
        self.closeButton.anchor(top: photosViewContainer.topAnchor, left: photosViewContainer.leftAnchor, bottom: nil, right: nil, paddingTop: 20, paddingLeft: 20, paddingBottom: 0, paddingRight: 0, width: 45, height:45, enableInsets: false)
        
    }
    
    func setupPageControl() {
        photosViewContainer.addSubview(pageControl)
        self.pageControl.centerXAnchor.constraint(equalTo: photosViewContainer.centerXAnchor).isActive = true
        
        self.pageControl.currentPageIndicatorTintColor = #colorLiteral(red: 1, green: 0.4303138256, blue: 0.07785933465, alpha: 1)
        self.pageControl.anchor(top: nil, left: nil, bottom: photosViewContainer.bottomAnchor, right: nil, paddingTop: 0, paddingLeft: 0, paddingBottom: 20, paddingRight: 0, width: 0, height:0, enableInsets: false)

    }
    func setupPhotosViewsContainer(){
        view.addSubview(photosViewContainer)
        self.photosViewContainer.anchor(top: view.topAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 300, enableInsets: false)
        
    }
    
    func setupScrollView() {
        self.scrollView.delegate = self
        self.scrollView.showsHorizontalScrollIndicator = false
        self.scrollView.isPagingEnabled = true

        photosViewContainer.addSubview(scrollView)
        self.scrollView.anchor(top: photosViewContainer.topAnchor, left: photosViewContainer.leftAnchor, bottom: photosViewContainer.bottomAnchor, right: photosViewContainer.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 0, enableInsets: false)
        
    }
}

extension ItemDetailViewController:UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        pageControl.currentPage = Int(self.scrollView.contentOffset.x / CGFloat(200))
    }
}
extension ItemDetailViewController:PresenterToViewItemDetailProtocol {
    
}



