//
//  ItemDetailPresenter.swift
//  LBCTest
//
//  Created by Malek BARKAOUI on 21/10/2020.
//

import Foundation

class ItemDetailPresenter: ViewToPresenterItemDetailProtocol {

    var view: PresenterToViewItemDetailProtocol?
    
    var interactor: PresenterToInteractorItemDetailProtocol?
    
    var router: PresenterToRouterItemDetailProtocol?
    
    var item: Item?
    
    func dismissItemDetail() {
        self.router?.dismiss(on: view!)
    }
    
    
}

extension ItemDetailPresenter: InteractorToPresenterItemDetailProtocol {
    
}

