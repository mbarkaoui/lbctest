//
//  ItemDetailRouter.swift
//  LBCTest
//
//  Created by Malek BARKAOUI on 21/10/2020.
//

import Foundation

class ItemDetailRouter:PresenterToRouterItemDetailProtocol {
    
    static func createItemDetailModule(item: Item?) -> ItemDetailViewController {
        let viewController = ItemDetailViewController()
        
        let presenter: ViewToPresenterItemDetailProtocol & InteractorToPresenterItemDetailProtocol = ItemDetailPresenter()
        
        presenter.item = item
        viewController.itemDetailPresenter = presenter
        viewController.itemDetailPresenter?.view = viewController
        viewController.itemDetailPresenter?.interactor = ItemDetailInteractor()
        viewController.itemDetailPresenter?.router = ItemDetailRouter()
        viewController.itemDetailPresenter?.interactor?.presenter = presenter

        return viewController
    }
    
    func dismiss(on view: PresenterToViewItemDetailProtocol) {
        let viewController = view as! ItemDetailViewController
        viewController.dismiss(animated: true, completion: nil)
    }
}
