//
//  LBCTestTests.swift
//  LBCTestTests
//
//  Created by Malek BARKAOUI on 19/10/2020.
//

import XCTest
@testable import LBCTest

class LBCTestTests: XCTestCase {
    var localDataManager : LocalDataManager!
    var itemsPresenter : ItemsListPresenter!

    
 
    override func setUpWithError() throws {
    localDataManager = LocalDataManager()
    itemsPresenter = ItemsListPresenter()
    }

    override func tearDownWithError() throws {
        localDataManager = nil
        itemsPresenter = nil
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testSortedNotUrgentItems() throws {
        
        guard let items = localDataManager.getItems() else { return  }
        
        let sortedItem = itemsPresenter.sortedNotUrgentItems(items: items)
        XCTAssertEqual(sortedItem[0].id ,1550491131)
        XCTAssertEqual(sortedItem[sortedItem.count-1].id,1661323991)
     
    }
    
    func testSortedUrgentItems() throws {
        
        guard let items = localDataManager.getItems() else { return  }
        
        let sortedItem = itemsPresenter.sortedUrgentItems(items: items)
        XCTAssertEqual(sortedItem.count, 102)
        XCTAssertEqual(sortedItem[0].id ,1547408955)
        XCTAssertEqual(sortedItem[sortedItem.count-1].id,1685978726)
    }
    
    func testUrgentItemsWithCategorie() throws {
        guard let items = localDataManager.getItems() else { return  }
        
        let itemsForCategory = itemsPresenter.getItemsWithCategory(items: items, categoryId: 2)
        XCTAssertEqual(itemsForCategory.count, 56)
        XCTAssertEqual(itemsForCategory[0].id ,1077103477)
        XCTAssertEqual(itemsForCategory[itemsForCategory.count-1].id ,1702181129)

    }
    
    func testAppendingUrgentNotUrgent(){
        guard let items = localDataManager.getItems() else { return  }
        
        let urgentItems = itemsPresenter.sortedUrgentItems(items: items)
        let notUrgentItems = itemsPresenter.sortedNotUrgentItems(items: items)

        let finalItemsList = itemsPresenter.appendUrgentNotUrgent(urgentItemList: urgentItems, notUrgentItems: notUrgentItems)
        
        XCTAssertEqual(finalItemsList[0].id ,1547408955)
        XCTAssertEqual(finalItemsList[urgentItems.count-1].id ,1685978726)
        XCTAssertEqual(finalItemsList[urgentItems.count].id ,1550491131)
        XCTAssertEqual(finalItemsList[(notUrgentItems.count)+(urgentItems.count-1)].id ,1661323991)
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
